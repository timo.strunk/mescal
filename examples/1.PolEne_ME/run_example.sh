
NCPU=2
MESCAL='../../src/mescal_mpi'


mpirun -np $NCPU $MESCAL neu.inp > neu.out
mpirun -np $NCPU $MESCAL cat.inp > cat.out
mpirun -np $NCPU $MESCAL ani.inp > ani.out



echo
echo 'Polarization energy for holes (eV units):'

U0=$(grep g_ENE neu.out | awk '{print $4}')
Up=$(grep g_ENE cat.out | awk '{print $4}')
D_tot=$(echo $U0 - $Up | bc)

U0=$(grep g_ELE neu.out | awk '{print $4}')
Up=$(grep g_ELE cat.out | awk '{print $4}')
D_els=$(echo $U0 - $Up | bc)

D_ind=$(echo $D_tot - $D_els | bc)

echo 'Total         :' $D_tot
echo 'Electrostatic :' $D_els
echo 'Induction     :' $D_ind

echo
echo
echo 'Polarization energy for electrons (eV units):'

U0=$(grep g_ENE neu.out | awk '{print $4}')
Um=$(grep g_ENE ani.out | awk '{print $4}')
D_tot=$(echo $U0 - $Um | bc)

U0=$(grep g_ELE neu.out | awk '{print $4}')
Um=$(grep g_ELE ani.out | awk '{print $4}')
D_els=$(echo $U0 - $Um | bc)

D_ind=$(echo $D_tot - $D_els | bc)

echo 'Total         :' $D_tot
echo 'Electrostatic :' $D_els
echo 'Induction     :' $D_ind

