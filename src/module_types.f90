!    This file is part of MESCal.
!    Copyright 2013-2015 Gabriele D'Avino
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module types 
  ! Global constants and variables, definitions of the derived data types
  implicit none

  SAVE

integer option

! real kind:
integer,  parameter :: rk=selected_real_kind(p=15)  ! double prec
!integer,  parameter :: rk=selected_real_kind(p=7)  ! single prec

! static memory allocation parameters (to modify if necessary)
! Consider dynamic allocation for more efficient memory usage
integer, parameter :: moltype_max=5,at_per_mol_max=150 !,nb_max=100000 ! 2clear

! some constants used in the program
real(rk),parameter ::  iu2ev=14.39964485_rk
real(rk),parameter ::  ev2iu=0.0694461572_rk
real(rk),parameter ::  eA2Debye=4.80320454_rk
real(rk),parameter ::  Debye2eA=0.20819434_rk
real(rk),parameter ::  epsilon=10.0_rk**(-15)
real(rk),parameter ::  eps0=8.854187817e-12

! Other global variables 
real(rk)      ::  energy_elstat,energy_induction,energy_total
real(rk)      ::  time_begin,time_end,time_io
integer       ::  max_it
real(rk)      ::  a_screen ! Screening factor 
real(rk)      ::  ks_kr,s_kr ! pars for r-deependent dielectric screening
real(rk)      ::  tolE,damp,damp_read,Emax
character(50) ::  root_fname
logical       ::  is_me,is_cr,is_intra_dipdip,screen_inter_dipdip
logical       ::  use_smart_damp,is_periodic_2D_Rc,is_periodic_3D_Rc
logical       ::  read_ext_VF_pc=.false.,round_chg=.false.
integer       ::  iP0_m1,iP1_m1,iP2_m1
real(rk)      ::  alpha_m1(3,3)
real(rk)      ::  cell2rep(3,3),Rc_rep  !,Rc_tf,Rc_sph 2clear
integer       ::  u_tf,n_rep,Nrep_tf
real(rk)      ::  F0,CellVolume,chi0(3) ! variables for calculations with periodic replica and external field
logical       ::  relax_dummy,is_dummy(moltype_max)=.false.
logical       ::  add_qm=.false.
character(80) ::  input_file(100)
integer       ::  nl_inp 
integer       ::  i_mirr ! image charges/dipoles plane 
real(rk)      ::  z_mirr 
logical       ::  is_mirr=.false.
logical       ::  use_pdx=.false.


! Allocatable arrays
real(rk),dimension(:,:),allocatable      ::  r_rep
integer,dimension(:,:),allocatable       ::  idx_rep

! Definition of derived data types
type molecule
   integer      :: moltype_id ! index to associate variables in sys_info and AAP
   integer      :: resid ! molecula index as read from pdb 
   real(rk)     :: r_at(at_per_mol_max,3) ! atomic coordinates

   real(rk)     :: q_0(at_per_mol_max)   ! permanent atomic charges (at step 0)
   real(rk)     :: q_i(at_per_mol_max)   ! total (perm + induced) charges at step i
   real(rk)     :: dq_i(at_per_mol_max)    ! Induced charges at step i
   real(rk)     :: dq_im1(at_per_mol_max) ! total (perm + induced) charges at step i-1

   real(rk)     :: mu_i(at_per_mol_max,3) ! induced dipoles at step i
   real(rk)     :: mu_im1(at_per_mol_max,3) ! induced dipoles at step i-1

   ! potential at atoms
   real(rk)     :: V_pc(at_per_mol_max)   ! permanent charges 
   real(rk)     :: V_ext(at_per_mol_max)  ! external sources 
   real(rk)     :: V_0(at_per_mol_max)    ! permanent sources: pc + ext 
   real(rk)     :: V_dq(at_per_mol_max)   ! induced charges
   real(rk)     :: V_id(at_per_mol_max)   ! induced dipoles
   real(rk)     :: V_tot(at_per_mol_max)  ! total: pc + ext + dq + id

  ! field at atoms
   real(rk)     :: F_pc(at_per_mol_max,3)   ! permanent charges 
   real(rk)     :: F_ext(at_per_mol_max,3)  ! external sources 
   real(rk)     :: F_0(at_per_mol_max,3)    ! permanent sources: pc + ext 
   real(rk)     :: F_dq(at_per_mol_max,3)   ! induced charges
   real(rk)     :: F_id(at_per_mol_max,3)   ! induced dipoles
   real(rk)     :: F_tot(at_per_mol_max,3)  ! total: pc + ext + dq + id

   ! other molecular variables
   real(rk)     :: acp(at_per_mol_max,6) ! atomic classic polarizability xx xy xz yy yz zz
   real(rk)     :: cm(3) ! molecular center of mass
   real(rk)     :: mu_mol(3) ! molecular dipole
   real(rk)     :: chg_mol   ! molecular charge
   real(rk)     :: screen_k(at_per_mol_max)
!   integer      :: nb_list(nb_max)
!   integer      :: nnb
end type molecule  


type sys_summary
   integer  :: mol_types  ! number of types of mol in the system
   integer  :: nat_tot    ! total number of atoms
   integer  :: nmol_tot   ! total number of molecules
   integer  :: at_per_mol_max
   real(rk) :: total_chg
   character(len=4),dimension(moltype_max) :: mol_names 
   ! name (residue name in pdb) of each molecule
   integer,dimension(moltype_max) :: nat_type
   ! number of atoms of each molecule
   integer,dimension(moltype_max) :: nmol_type
   ! number of molecules of each type
   real(rk),dimension(moltype_max) :: chg_type
   real(rk)  :: dipole_tot(3)
!   character(len=40),dimension(moltype_max) :: parfiles !2clear
   real(rk)   :: chg_diff(at_per_mol_max,moltype_max) ! difference of atomc charges 
   character(len=40),dimension(moltype_max) :: mei_fn
!   character(len=40),dimension(moltype_max) :: mei_fn_alt
   character(len=40),dimension(moltype_max) :: aap_fn
end type sys_summary



end module types

