!    This file is part of MESCal.
!    Copyright 2013-2015 Gabriele D'Avino
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module microelectrostatic
  ! Options/subroutines for microelectrostatic calculations, i.e.  
  ! polarization via induced dipoles only
!  use io
  use types
  use math
  use electrostatics
  use selfconsistent
  
#ifdef MPI
  use module_mpi
#endif
  
  implicit none

contains


subroutine  driver_MECR 
! GD 27/10/15 BIG NEWS!
! New driver routine for ALL ME and CR calculations.
! All previous routines (ext field or pbc) are disabled! 
  implicit none
  character(80) :: msg(5),fn_ext
  character(50) :: key,job_type
  logical       :: found,use_spec_chg
  real(rk)      :: F_ext_uni(3)
  integer       :: Npbc
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef MPI
  if (proc_id==0) then
#endif
job_type=''
key="JOBTYPE"
call parse_str(key,job_type,found) 

if (job_type=='CR') then
   is_cr=.true.
   is_me=.false.
   is_intra_dipdip=.false.
   screen_inter_dipdip=.false.
   msg(1)="SELF-CONSISTENT CHARGE REDISTRIBUTION CALCULATION"

elseif (job_type=='ME') then
   is_me=.true.
   is_cr=.false.
   msg(1)="SELF-CONSISTENT MICROELECTROSTATIC CALCULATION"

endif
call option_banner(msg,1)


call system_input(sys_info,pdbfile)
call read_atomic_coord(pdbfile,sys)
call read_input_fnames


call input_round 
if (job_type=='CR') then
   call input_sccr(sys_info,aap,n_crp,i_crp)
   call input_pp_general(sys_info,n_pp,i_pp)
elseif (job_type=='ME') then
   call input_chg_me(sys_info,n_crp,i_crp)
   call input_pp_general(sys_info,n_pp,i_pp)
   call input_screen
endif


key="SPEC_CHG"
call parse_log(key,use_spec_chg,found)
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',use_spec_chg
else
   use_spec_chg=.false.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',use_spec_chg,' (default)'
endif
write(*,*)
if (use_spec_chg) call load_specific_charges


!2c call cutoff_nblist 
call sc_input 


! add uniform external field
F_ext_uni=0.0_rk
key="FEXT_UNI"
call parse_real3(key,F_ext_uni,found) 
if (found) then
   write(*,*) key,': ',F_ext_uni
   write(*,*) 'INFO: Applying an external uniform electric field!'
   write(*,*)
   call set_Fext_uni(F_ext_uni)
else 
   call set_Fext_zero
endif


! Add non-uniform external field 
fn_ext=''
key="FEXT"
call parse_str(key,fn_ext,found) 
if (found) then
   write(*,*) key,': ',fn_ext
   write(*,*) 'INFO: Applying an external non-uniform electric field!'
   write(*,*)
   call add_Fext_notuni(fn_ext)
endif

fn_ext=''
key="FEXT2"
call parse_str(key,fn_ext,found) 
if (found) then
   write(*,*) key,': ',fn_ext
   write(*,*) 'INFO: Applying an external non-uniform electric field!'
   write(*,*)
   call add_Fext_notuni(fn_ext)
endif


! Consider periodic boundary conditions?
is_periodic_2D_Rc=.false. 
is_periodic_3D_Rc=.false.
Npbc=0
key="PBC"
call parse_int(key,Npbc,found)
if (found) then 
   write(*,*) key,': ',Npbc

   if (Npbc==2) then
      write(*,*) 'INFO: PBC in 2D have been enabled!'
      is_periodic_2D_Rc=.true. 
      is_periodic_3D_Rc=.false.
      call build_replica_2D
      
   elseif (Npbc==3) then
      write(*,*) 'INFO: PBC in 3D have been enabled!'
      is_periodic_2D_Rc=.false. 
      is_periodic_3D_Rc=.true.
      call build_replica_3D
      
   else
      write(*,*) ' ERROR: Input value not contemplated! '
      call stop_exec       

   endif
   write(*,*)
endif

! Consider image charges/dipole?
i_mirr=0
z_mirr=0.0
is_mirr=.false. 
key="MIRROR"
call parse_int_real(key,i_mirr,z_mirr,found)
if (found) then 
   write(*,"(1x,a20,a2,i3,1x,f10.4)") key,': ',i_mirr,z_mirr
   is_mirr=.true. 

   if ( (i_mirr.lt.1) .or. (i_mirr.gt.3) ) then
      write(*,*) " Value not contemplated!!! "
      call stop_exec
   elseif ( (is_periodic_2D_Rc) .or. (is_periodic_3D_Rc) ) then
      write(*,*) " Image charges/dipoles are not compatible with PBC!!! "
      call stop_exec

   elseif ( (is_intra_dipdip) .or. (screen_inter_dipdip) ) then
      write(*,*) "Image charges/dipoles are not compatible with intra/inter-molecular screening!!! "
      call stop_exec
   endif
      

   write(*,*)
   write(*,*) 'INFO: Image charges and dipoles have been enabled! '
   key='xyz'
   write(*,*) 'Mirror plane: ',key(i_mirr:i_mirr),'=',z_mirr
   write(*,*)
endif

call IOfinished_banner

#ifdef MPI
  endif
#endif
call selfconsistent_solution

end subroutine driver_MECR



subroutine  opt_VF_pc_cutoff 
! GD 3/12/15: modified for the flexible input version.
! introducing pbc in 2D and 3D
  implicit none
  integer       :: Npbc
  character(80) :: msg(5)
  character(50) :: key
  character(150):: fn_out
  logical       :: found,use_spec_chg


msg(1)="PERFORM ELECTROSTATIC SUMS ACCOUNTING FOR PBC WITHIN CUTOFF"
call option_banner(msg,1)

is_cr=.false.
is_me=.true.

! input-output 
call system_input(sys_info,pdbfile)
call read_atomic_coord(pdbfile,sys)
call read_input_fnames
call input_chg_me(sys_info,n_crp,i_crp)
call input_pp_general(sys_info,n_pp,i_pp)
!2c call cutoff_nblist


! GD 8/2/17: commented and replaced, see below
!key="SPEC_CHG"
!call parse_log(key,use_spec_chg,found)
!write(*,*) key,': ',use_spec_chg
!write(*,*)
!if (use_spec_chg) call load_specific_charges

key="SPEC_CHG"
call parse_log(key,use_spec_chg,found)
if (found) then 
   write(*,"(1x,a20,a2,l1)") key,': ',use_spec_chg
else
   use_spec_chg=.false.
   write(*,"(1x,a20,a2,l1,a10)") key,': ',use_spec_chg,' (default)'
endif
write(*,*)
if (use_spec_chg) call load_specific_charges




! Consider periodic boundary conditions?
is_periodic_2D_Rc=.false. 
is_periodic_3D_Rc=.false.
Npbc=0
key="PBC"
call parse_int(key,Npbc,found)
if (found) then 
   write(*,*) key,': ',Npbc

   if (Npbc==2) then
      write(*,*) 'INFO: PBC in 2D have been enabled!'
      is_periodic_2D_Rc=.true. 
      is_periodic_3D_Rc=.false.
      call input_replica_2D
      
   elseif (Npbc==3) then
      write(*,*) 'INFO: PBC in 3D have been enabled!'
      is_periodic_2D_Rc=.false. 
      is_periodic_3D_Rc=.true.
      call input_replica_3D
      
   else
      write(*,*) ' ERROR: Input value not contemplated! '
      call stop_exec       

   endif
   write(*,*)
endif


call calc_VF_chg_cutoff

call calc_V0_F0
call calc_V_tot



!!! FIELDS AND POTENTIALS
fn_out=''
key="OUT_RST"
call parse_str_long(key,fn_out,found) 
if (found) then
   write(*,*) key,': ',fn_out
   write(*,*)

   call write_q_mu_V_F(fn_out)
   write(*,*) 
endif


!!! ELECTROSTATIC SITE ENERGIES
fn_out=''
key="SITE_ENERGY_ELSTAT"
call parse_str_long(key,fn_out,found) 
if (found) then
   write(*,*) key,': ',adjustl(trim(fn_out))
   write(*,*)


   call input_chg_alt


   call sitene_elstat(fn_out)

   write(*,*) 'INFO: Electrostatic site energies written to file'
   write(*,*) 
endif



end subroutine opt_VF_pc_cutoff



subroutine opt_r_screen
! Electrostatic calculations with r-dependent screening 
  implicit none
  real(rk)    :: r1(3),r2(3),q_tot,ene

write(*,*) ' .................................. let'//achar(39)//'s go! '
write(*,*) ' '
write(*,*) ' You choose option ', option
write(*,*) ' Electrostatic calculation with r-dependent screening' 
write(*,*) ' '
is_me=.true.
is_cr=.false.
!is_rst=.false.
is_periodic_2D_Rc=.false.
is_periodic_3D_Rc=.false.

! input-output 
call system_input(sys_info,pdbfile)
call read_atomic_coord (pdbfile,sys)
call read_input_fnames
call input_chg_me(sys_info,n_crp,i_crp)
!call cutoff_nblist  
call input_kr
call set_Fext_zero
call IOfinished_banner
!call check_io 


call calc_V_chg_kr
call calc_energy_q_only(ene) 


ene=ene*iu2ev
write(*,*) '  '
write(*,*) '  *** Electrostatic calculation with r-dependent screening done! '
write(*,*) ' '
write(*,"(a,f16.4,a)") '   System Energy (eV) ', ene,'  (Electrostatic Energy) g_ELSTAT '

end subroutine opt_r_screen



subroutine opt_ME_write_rst
  ! Write induced dipoles for restart porpouse
  implicit none

write(*,*) ' '
write(*,*) ' You choose option ', option
write(*,*) ' Write restart file for ME calculation.' 
write(*,*) ' '

call write_dipoles

end subroutine opt_ME_write_rst



subroutine opt_scale_apha_at
  implicit none
  real(rk)    :: r1(3),r2(3),q_tot,FF,tol_a
  real(rk)    :: U(3,3),P0(3),P1(3),P2(3),ind_dip(3),scale_factor(3,3)
  real(rk)    :: alpha_mol(6),alpha_mol_mat(3,3),alpha_at(3,3)
  real(rk)    :: alpha_out(3,3),alpha_target(3,3),diff_alpha(3,3),diff_rel(3,3)
  integer     :: i_sca,i,maxit_sca,i_mol,mtype,i_at

write(*,*) ' .................................. let'//achar(39)//'s go! '
write(*,*) ' '
write(*,*) ' You choose option ', option
write(*,*) ' Self-consitent rescaling of distributed atomic polarizabilities '
write(*,*) ' for ME calculations with screened intramolecular dipole-dipole interactions.'
is_me=.true.
is_cr=.false.
is_periodic_2D_Rc=.false.
is_periodic_3D_Rc=.false.

! input-output
call system_input(sys_info,pdbfile)

if (sys_info%nmol_tot.ne.1) then
   write(*,*) 'ERROR: This option is valid only for systems composed of one molecule'
   call stop_exec
endif

call read_atomic_coord (pdbfile,sys)
call read_input_fnames
call input_chg_me(sys_info,n_crp,i_crp)
call input_pp_general(sys_info,n_pp,i_pp)
call set_Fext_zero
!2c call cutoff_nblist
call sc_input
call input_screen
call IOfinished_banner

FF=ev2iu*0.01_rk  ! value of finite field
maxit_sca=100

i_mol=1
mtype=sys(i_mol)%moltype_id
P0=sys(i_mol)%r_at(iP0_m1,:)  
P1=sys(i_mol)%r_at(iP1_m1,:)
P2=sys(i_mol)%r_at(iP2_m1,:)
U=0.0_rk
call find_rotU(U,P0,P1,P2)
! colums of U give the three directions of the applied field  
write(*,*) 'U:'
call print_mat3(U) 
write(*,*)

write(*,*)
write(*,*) ' INFO: the input alpha (ALPHA_INP) and difference between target      '
write(*,*) '       and measured values (DIFF) are printed at every iterative step '
write(*,*)

! lab frame
call calc_mol_pol(alpha_mol) 
alpha_mol_mat=uptriangle_2_mat3(alpha_mol)
alpha_target=matmul(matmul(transpose(U),alpha_mol_mat),U) ! rotation  to mol frame
!write(*,*) ' ROTATED alpha target summing over atoms:'
!call print_mat3(alpha_mol_mat)

tol_a=5e-3 ! tolerance on alpha value / Angs^3
do i_sca=1,maxit_sca 
   alpha_out=0.0_rk

! computing and writing input alpha
   call calc_mol_pol(alpha_mol)
   alpha_mol_mat=uptriangle_2_mat3(alpha_mol)
   alpha_mol_mat=matmul(matmul(transpose(U),alpha_mol_mat),U)
   alpha_mol=mat3_2_uptriangleup(alpha_mol_mat)
   
   
   do i=1,3 ! field directions

      call set_Fext_uni(FF*U(:,i))
      call selfconsistent_solution
      call calc_dipole_pp(ind_dip)
      alpha_out(:,i)=matmul(transpose(U),ind_dip)/FF  ! molecular frame

   enddo

   ! averaging off-diagonal components
   alpha_out(1,2)=0.5_rk*(alpha_out(1,2)+alpha_out(2,1))
   alpha_out(2,1)=alpha_out(1,2)
   alpha_out(1,3)=0.5_rk*(alpha_out(1,3)+alpha_out(3,1))
   alpha_out(3,1)=alpha_out(1,3)
   alpha_out(2,3)=0.5_rk*(alpha_out(2,3)+alpha_out(3,2))
   alpha_out(3,2)=alpha_out(2,3)

   ! calculating differences and printing iteration log
   diff_alpha=alpha_target-alpha_out

   write(*,*) 
   write(*,"(a,i3,a)") '=======  Polarizability Rescaling - Iteration ',i_sca,' ======'
   write(*,"(6(f8.3),a)") alpha_mol,'  ALPHA_INP'                       ! molecular frame
   write(*,"(6(f8.3),a)") mat3_2_uptriangleup(alpha_out),'  ALPHA_OUT'  ! mol frame
   write(*,"(6(f8.3),a)") diff_alpha(1,1),diff_alpha(1,2),diff_alpha(1,3), &
                          diff_alpha(2,2),diff_alpha(2,3),diff_alpha(3,3),'  DIFF'
   write(*,*) 

   scale_factor=alpha_target/(alpha_out+epsilon)

!   write(*,"(6(f8.3),a)") mat3_2_uptriangleup(alpha_mol_mat*scale_factor),'  ALPHA_INP_NEXT'
!   write(*,*) 'Scale factor rotation: '
!   call print_mat3(scale_factor)

   ! reassignement of rescaled polarizability
   do i_at=1,n_pp(i_mol) 
      alpha_at=uptriangle_2_mat3(sys(i_mol)%acp(i_at,:))
      alpha_at=matmul(matmul(transpose(U),alpha_at),U)
      alpha_at=alpha_at*scale_factor
      alpha_at=matmul(matmul(U,alpha_at),transpose(U))
      sys(i_mol)%acp(i_at,:)=mat3_2_uptriangleup(alpha_at)
   enddo




   if ( (abs(diff_alpha(1,1)).lt.tol_a) .and. &
        (abs(diff_alpha(2,2)).lt.tol_a) .and. &
        (abs(diff_alpha(3,3)).lt.tol_a) ) then

      write(*,*)
      write(*,"(a,f6.3,a)") 'Target polarizability reached for screening factor of ',a_screen,'!'
      write(*,*)
      write(*,"(a)")  'Polarizability (diagonal only) value to give as input is:'

      write(*,"(6(f8.3),a)") alpha_mol(1:6),'  ALPHA_FINAL_FULL'
      write(*,"(6(f8.3),a)") alpha_mol(1),0.0,0.0,alpha_mol(4),0.0,alpha_mol(6),'  ALPHA_FINAL_DIAG'
      write(*,*)
      write(*,*) ' INFO: small (<1-2% of alpha_iso) off-diagonal elements      '
      write(*,*) '       (resulting from a non perfect alignment of molecular  '
      write(*,*) '       frame and the principal axes of alpha tensor) can be  '
      write(*,*) '       neglected. '
      write(*,*)

      exit
   endif

enddo

contains 

  subroutine calc_mol_pol(mol_pol)
! compute alpha_mol as sum of individual alpha in the lab frame
    implicit none
    real(rk),intent(out)    :: mol_pol(6)

    mol_pol(1)=sum(sys(i_mol)%acp(1:n_pp(1),1))
    mol_pol(2)=sum(sys(i_mol)%acp(1:n_pp(1),2))
    mol_pol(3)=sum(sys(i_mol)%acp(1:n_pp(1),3))
    mol_pol(4)=sum(sys(i_mol)%acp(1:n_pp(1),4))
    mol_pol(5)=sum(sys(i_mol)%acp(1:n_pp(1),5))
    mol_pol(6)=sum(sys(i_mol)%acp(1:n_pp(1),6))

  end subroutine calc_mol_pol


end subroutine opt_scale_apha_at


end module microelectrostatic






