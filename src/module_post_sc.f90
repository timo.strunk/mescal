!    This file is part of MESCal.
!    Copyright 2013-2015 Gabriele D'Avino
!
!    MESCal is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License.
!
!    MESCal is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MESCal.  If not, see <http://www.gnu.org/licenses/>.
!
module post_sc
  ! Subroutine for calculations once self-consistent solution is achieved
  use io
  use types
  use math
  use electrostatics 

contains


! 2clear
!subroutine opt_write_dip_2q
!! Write induces dipoles as couples of point charges'
!  implicit none
!  character(40)    :: fout_name
!
!write(*,*) ' '
!write(*,*) ' You choose option ', option
!write(*,*) ' Write induces dipoles as pairs of point charges '
!write(*,*) ' '
!
!fout_name='dipoles_as_pc.dat'
!
!call write_dip2pc(fout_name)
!write(*,*) 'Induced dipoles  wrote to ',trim(fout_name)
!write(*,*)
!do i=1,sys_info%nmol_tot  ! loop on all molecules
!   i_type=sys(i)%moltype_id
!     
!   do j=1,n_pp(i_type)  ! loop on pp 
!
!      r_mu=sys(i)%r_at(j,:)
!      mu=sys(i)%mu_i(j,:)
!      mu_abs=Norm3(sys(i)%mu_i(j,:) )
!      mu_v=mu/mu_abs
!      q=mu_abs/d
!
!      write(49,"(4(f15.6))") r_mu+d2*mu_v,q
!      write(49,"(4(f15.6))") r_mu-d2*mu_v,-q
!   enddo
!enddo
!close(49)
!
!end subroutine opt_write_dip_2q


! 2clear
!subroutine opt_write_qmVF
!  implicit none
!  character(40)    :: fout_name
!
!write(*,*) ' '
!write(*,*) ' You choose option ', option
!write(*,*) ' Write to file charges, induced dipoles, field at potential at all atoms.'
!write(*,*) ' This file can be used for restart purpose.'
!write(*,*) ' '
!write(*,*) ' '
!write(*,*) ' Insert name of output file: '
!read(*,*)  fout_name
!write(*,*) ' >> ',trim(fout_name),' read'
!write(*,*) ' '
!
!call write_q_mu_V_F(fout_name) 
!
!
!write(*,*) 'Data wrote to ',trim(fout_name)
!write(*,*)
!
!end subroutine opt_write_qmVF


subroutine opt_write_dip_sel
! This option computes and writes the dipole of selected molecules
  implicit none
  character(40)   :: f_list,line 
  logical         :: list_exists
  integer         :: stat,n_mol_dip,i,i_mol
  integer,allocatable,dimension(:) :: list_dip
  real(rk)   :: dip_selection(3)

allocate(list_dip(sys_info%nmol_tot))


write(*,*) ' '
write(*,*) ' You choose option ', option
write(*,*) ' Compute and write the dipole of selected molecules.'
write(*,*) ' '

write(*,*) ' Insert filename with list of molecular selection   '
write(*,*) ' Accepted file format is one molecular index per row! '
read(*,*)  f_list
write(*,*) ' >> ',trim(f_list),' read'
write(*,*) ' '
inquire(file=f_list,exist=list_exists)

n_mol_dip=0
list_dip=0
if (list_exists) then
   open(file=f_list,unit=45,iostat=stat,status="old")
   line=""

   do while(stat==0)
      read(45,"(a40)",iostat=stat) line

      if (stat==0) then
         n_mol_dip=n_mol_dip+1
         read(line,*)  list_dip(n_mol_dip)
      endif

   enddo
else
   write(*,*) "ERROR: File "//f_list//" not found!"
   call stop_exec
endif
close(45)

! check
!do i=1,n_mol_dip
!  write(*,*) list_dip(i)
!enddo
dip_selection=0.0d0
do i=1,n_mol_dip
   i_mol=list_dip(i)
   dip_selection=dip_selection + sys(i_mol)%mu_mol
enddo

write(*,*) 'Selected molecules are:'
write(*,"(10(1x,i6))") (list_dip(i),i=1,n_mol_dip)

write(*,*) 
write(*,"(a,3(f18.7),a)") ' Dipole of selected molecules (Debye): ',dip_selection*eA2Debye,' g_DIPSEL'
write(*,*) 

!!!!!!!!!!!!!

end subroutine opt_write_dip_sel


! GD 31/12/2017 
subroutine opt_write_chg_dip
  implicit none
  character(80)   :: msg(5)
  character(50)   :: key,fout_name
  integer         :: i,j,i_type
  real(rk)        :: d,d2,q,r_mu(3),mu(3),mu_v(3),mu_abs
  logical         :: found

d=0.1_rk
d2=d*0.5_rk

key="OUT_CHG_DIP"
call parse_str(key,fout_name,found)
if (found) then
   write(*,"(1x,a20,a2,a40)") key,': ',fout_name
   write(*,*)
else
   write(*,*) '  ERROR: something went wrong!'
   call stop_exec
endif

msg=''
msg(1)="WRITE TO FILE ATOMIC CHARGES AND DIPOLES"
msg(2)="DIPOLES ARE WRITTEN AS PAIRS OF CHARGES"
call option_banner(msg,2)

open(file=fout_name,unit=45,status="replace")
write(45,"(a)") '% 1-3.r(Ang)  4.q (e units)'


do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id
     
   do j=1,n_pp(i_type)  ! loop on pp 
      
      r_mu=sys(i)%r_at(j,:)
      mu=sys(i)%mu_i(j,:)
      mu_abs=Norm3(sys(i)%mu_i(j,:) )
      mu_v=mu/mu_abs
      q=mu_abs/d

      write(45,"(4(1x,f15.8))") r_mu,sys(i)%q_i(j)
      write(45,"(4(1x,f15.8))") r_mu+d2*mu_v,q
      write(45,"(4(1x,f15.8))") r_mu-d2*mu_v,-q
   enddo
enddo

write(*,*) '  Charges and induced dipoles wrote to file: ',trim(fout_name)
write(*,*)
write(*,*)

close(45)


end subroutine opt_write_chg_dip


! GD 10/5/19 
! does the same job as opt_write_chg_dip but writes the output in
! format suitable for MM embeding in ORCA 
subroutine opt_write_chg_dip_mm
  implicit none
  character(80)   :: msg(5)
  character(50)   :: key,fout_name
  integer         :: i,j,i_type,i_rep
  real(rk)        :: d,d2,q,r_mu(3),mu(3),mu_v(3),mu_abs,qq
  logical         :: found

d=0.1_rk
d2=d*0.5_rk

key="OUT_CHG_DIP_MM"
call parse_str(key,fout_name,found)
if (found) then
   write(*,"(1x,a20,a2,a40)") key,': ',fout_name
   write(*,*)
else
   write(*,*) '  ERROR: something went wrong!'
   call stop_exec
endif

msg=''
msg(1)="WRITE TO FILE ATOMIC CHARGES AND DIPOLES"
msg(2)="DIPOLES ARE WRITTEN AS PAIRS OF CHARGES"
msg(3)="FORMAT SUITABLE FOR EMBEDDED ORCA CALCULATION"
call option_banner(msg,3)

open(file=fout_name,unit=45,status="replace")


! GD 25/7/19 Adding periodic replica withing cutoff 
if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then

   write(*,*) '  INFO: Also periodic replica within cutoff will be written to file!'
   write(*,*)
   write(45,"(i9)") 3*(sys_info%nat_tot + n_rep)
else

   write(45,"(i9)") 3*sys_info%nat_tot
endif

do i=1,sys_info%nmol_tot  ! loop on all molecules
   i_type=sys(i)%moltype_id
     
   do j=1,n_pp(i_type)  ! loop on pp 
      
      r_mu=sys(i)%r_at(j,:)
      mu=sys(i)%mu_i(j,:)
      mu_abs=Norm3(mu) 
      mu_v=mu/mu_abs
      q=mu_abs/d
      qq=sys(i)%q_0(j)+sys(i)%dq_i(j) ! neeeded as restarts don't read q0

      write(45,"(4(1x,f15.8))") qq,r_mu
      write(45,"(4(1x,f15.8))")  q,r_mu+d2*mu_v
      write(45,"(4(1x,f15.8))") -q,r_mu-d2*mu_v
   enddo
enddo




! GD 25/7/19 Adding periodic replica withing cutoff 

if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then

   do i_rep=1,n_rep

      r_mu=r_rep(i_rep,:)
      mu=sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:)
      mu_abs=Norm3(mu)
      mu_v=mu/mu_abs
      q=mu_abs/d

      ! neeeded as restarts don't read q0
      qq=sys(idx_rep(i_rep,2))%q_0(idx_rep(i_rep,3)) + &
         sys(idx_rep(i_rep,2))%dq_i(idx_rep(i_rep,3)) 

      write(45,"(4(1x,f15.8))") qq,r_mu
      write(45,"(4(1x,f15.8))")  q,r_mu+d2*mu_v
      write(45,"(4(1x,f15.8))") -q,r_mu-d2*mu_v

   enddo

endif


write(*,*) '  Charges and induced dipoles wrote to file: ',trim(fout_name)
write(*,*)
write(*,*)

close(45)


end subroutine opt_write_chg_dip_mm





! GD 22/04/16 new flexible-input version
subroutine opt_write_VF_at_r
  implicit none
  character(80)   :: msg(5)
  character(50)   :: f_list,line,rVFfmt
  logical         :: list_exists,found
  integer         :: stat,i_mol,i_type,l_pp,l_crp,i_rep,mol_excl,strlen
  real(rk)        :: coor(3),potential,field(3),q_rep,mu_rep(3)
  character(50)   :: key,fout_name

rVFfmt="(3(1x,f8.3),4(1x,E12.5))"

key="VF_POINTS"
f_list=''
call parse_str(key,f_list,found)
if (found) then
   write(*,"(1x,a20,a2,a40)") key,': ',f_list
   write(*,*)
else
   write(*,*) '  ERROR: something went wrong!'
   call stop_exec
endif
msg(1)="COMPUTE FIELD AND POTENTIAL AT SELECTED POINTS"
call option_banner(msg,1)

inquire(file=f_list,exist=list_exists)


strlen=len(trim(f_list))
fout_name=adjustl(f_list)
fout_name=fout_name(1:strlen-4)//'_vf.dat'
open(file=fout_name,unit=45,status="replace")

mol_excl=-1 ! do not exclude any molecule
if (list_exists) then
   open(file=f_list,unit=46,iostat=stat,status="old")
   line=""
   write(45,"(a)") '% 1-3.r(Ang)  4.V(Volt)  5-7.F(V/Ang) '

   do while(stat==0)
      line=''
      read(46,"(80a)",iostat=stat) line
!         write(*,*) 'line ',line

      if (stat==0) then 
         read(line,*) coor
         !write(*,*) 'r',coor
      
         potential=0.0d0
         field=0.0d0
         do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
            i_type=sys(i_mol)%moltype_id
            if (i_mol.ne.mol_excl) then

         !!!!!  Contribution from point charges
            do l_crp=1,n_crp(i_type) 
               potential=potential + &
                    Pot_chg(coor,sys(i_mol)%r_at(i_crp(l_crp,i_type),:), &
                    sys(i_mol)%q_i(l_crp)  )

               field=field + & 
                    Field_chg(coor,sys(i_mol)%r_at(i_crp(l_crp,i_type),:), &
                    sys(i_mol)%q_i(l_crp)  )
            enddo

         !!!!! Contribution from dipoles
            do l_pp=1,n_pp(i_type)
               potential=potential + &
                    Pot_dip(coor,sys(i_mol)%r_at(i_pp(l_pp,i_type),:),   &
                    sys(i_mol)%mu_i(l_pp,:)  )
               
               field=field + & 
                    Field_dip(coor,sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
                    sys(i_mol)%mu_i(l_pp,:)  )
            enddo

         endif
      enddo


         ! field and potential due to replicas
         if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  

            do i_rep=1,n_rep
               !           mol idx             ! atomic idx 
               q_rep=sys(idx_rep(i_rep,2))%q_i(idx_rep(i_rep,3))
               mu_rep=sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:) 

               potential=potential +  &
                    Pot_chg(coor,r_rep(i_rep,:),q_rep) + &
                    Pot_dip(coor,r_rep(i_rep,:),mu_rep) 

               field=field + &
                    Field_chg(coor,r_rep(i_rep,:),q_rep) + &
                    Field_dip(coor,r_rep(i_rep,:),mu_rep) 
               
            enddo
         endif


         write(45,rVFfmt) coor,iu2ev*potential,iu2ev*field

      endif
   enddo

else
   write(*,*) "ERROR: File "//f_list//" not found!"
   call stop_exec
endif

close(45)
close(46)

write(*,*) 
write(*,*) 'Electric field and potential wrote to file ',fout_name
write(*,*) 


end subroutine opt_write_VF_at_r



! GD 22/04/16 
! replaced by the new flexible-input routine
!subroutine opt_write_VF_at_r
!  implicit none
!  character(40)   :: f_list,line,rVFfmt
!  logical         :: list_exists
!  integer         :: stat,i_mol,i_type,l_pp,l_crp,i_rep,mol_excl
!  real(rk)        :: coor(3),potential,field(3),q_rep,mu_rep(3)
!  character(40)   :: fout_name
!
!rVFfmt="(3(1x,f8.3),4(1x,E12.5))"
!
!write(*,*) ' '
!write(*,*) ' You choose option ', option
!write(*,*) ' Write field and potential at selected positions'
!write(*,*) ' '
!
!write(*,*) ' Insert filename with list of sites '
!write(*,*) ' Accepted file format is one position (xyz coordinates) per row! '
!read(*,*)  f_list
!write(*,*) ' >> ',trim(f_list),' read'
!write(*,*) ' '
!inquire(file=f_list,exist=list_exists)
!
!write(*,*) ' Do you want to exclude one molecule? '
!write(*,*) ' Insert the molecular index, or 0 not to exculude any molecule.'
!read(*,*)  mol_excl
!write(*,*) ' >> ',mol_excl,' read'
!write(*,*) ' '
!
!
!write(*,*) ' '
!write(*,*) ' '
!write(*,*) ' Insert name of output file: '
!read(*,*)  fout_name
!write(*,*) ' >> ',trim(fout_name),' read'
!write(*,*) ' '
!
!open(file=fout_name,unit=45,status="replace")
!
!
!if (list_exists) then
!   open(file=f_list,unit=46,iostat=stat,status="old")
!   line=""
!   write(45,"(a)") '% 1-3.r(Ang)  4.V(Volt)  5-7.F(V/Ang) '
!
!   do while(stat==0)
!      read(46,"(80a)",iostat=stat) line
!         !write(*,*) 'line',line
!
!      if (stat==0) then 
!         read(line,*) coor
!         !write(*,*) 'r',coor
!      
!         potential=0.0d0
!         field=0.0d0
!         do i_mol=1,sys_info%nmol_tot  ! loop on all molecules
!            i_type=sys(i_mol)%moltype_id
!            if (i_mol.ne.mol_excl) then
!
!         !!!!!  Contribution from point charges
!            do l_crp=1,n_crp(i_type) 
!               potential=potential + &
!                    Pot_chg(coor,sys(i_mol)%r_at(i_crp(l_crp,i_type),:), &
!                    sys(i_mol)%q_i(l_crp)  )
!
!               field=field + & 
!                    Field_chg(coor,sys(i_mol)%r_at(i_crp(l_crp,i_type),:), &
!                    sys(i_mol)%q_i(l_crp)  )
!            enddo
!
!         !!!!! Contribution from dipoles
!            do l_pp=1,n_pp(i_type)
!               potential=potential + &
!                    Pot_dip(coor,sys(i_mol)%r_at(i_pp(l_pp,i_type),:),   &
!                    sys(i_mol)%mu_i(l_pp,:)  )
!               
!               field=field + & 
!                    Field_dip(coor,sys(i_mol)%r_at(i_pp(l_pp,i_type),:), &
!                    sys(i_mol)%mu_i(l_pp,:)  )
!            enddo
!
!         endif
!      enddo
!
!
!         ! field and potential due to replicas
!         if (is_periodic_2D_Rc.or.is_periodic_3D_Rc) then  
!
!            do i_rep=1,n_rep
!               !           mol idx             ! atomic idx 
!               q_rep=sys(idx_rep(i_rep,2))%q_i(idx_rep(i_rep,3))
!               mu_rep=sys(idx_rep(i_rep,2))%mu_i(idx_rep(i_rep,3),:) 
!
!               potential=potential +  &
!                    Pot_chg(coor,r_rep(i_rep,:),q_rep) + &
!                    Pot_dip(coor,r_rep(i_rep,:),mu_rep) 
!
!               field=field + &
!                    Field_chg(coor,r_rep(i_rep,:),q_rep) + &
!                    Field_dip(coor,r_rep(i_rep,:),mu_rep) 
!               
!            enddo
!         endif
!
!
!         write(45,rVFfmt) coor,iu2ev*potential,iu2ev*field
!
!      endif
!   enddo
!
!else
!   write(*,*) "ERROR: File "//f_list//" not found!"
!   call stop_exec
!endif
!
!close(46)
!write(*,*) 
!write(*,*) 'Electric field and potential wrote to field_pot.dat '
!write(*,*) 
!
!close(45)
!
!
!end subroutine opt_write_VF_at_r


end module post_sc
