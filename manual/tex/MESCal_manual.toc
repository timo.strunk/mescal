\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}Acknowledgments}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Copyrights and disclaimers}{2}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Program overview}{3}{subsection.1.3}
\contentsline {section}{\numberline {2}The MESCal\ input file}{5}{section.2}
\contentsline {subsubsection}{\numberline {2.0.1}List of available input records}{5}{subsubsection.2.0.1}
\contentsline {section}{\numberline {3}Molecular input files}{10}{section.3}
\contentsline {subsection}{\numberline {3.1}Microelectostatics input (mei) file}{10}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Rigid-molecule mei file}{10}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}Free-input mei file}{14}{subsubsection.3.1.2}
\contentsline {subsubsection}{\numberline {3.1.3}One-site molecule mei file}{16}{subsubsection.3.1.3}
\contentsline {subsection}{\numberline {3.2}Charge redistribution input (cri) and atom-atom polarizability (aap) files}{16}{subsection.3.2}
\contentsline {section}{\numberline {4}Compiling MESCal}{18}{section.4}
\contentsline {section}{\numberline {5}Running MESCal}{18}{section.5}
\contentsline {section}{\numberline {6}Calculation of polarization energies}{19}{section.6}
